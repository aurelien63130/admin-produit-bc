<?php
    function bddConnect(){
        try {
            $bdd = new PDO("mysql:dbname=animalerie;host=database", "root", "tiger");
            $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $bdd;
        } catch (\Exception $e){
            echo('Impossible de se connecter ! ');
            throw $e;
        }
    }
?>