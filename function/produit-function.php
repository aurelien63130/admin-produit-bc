<?php
function getProduitById($bdd, $id)
{
    $query = $bdd->prepare("SELECT * FROM produit WHERE id = :id");
    $query->execute(["id" => $id]);
    $produit = $query->fetch();

    return $produit;
}

function removeProductById($bdd, $id)
{
    $query = $bdd->prepare("DELETE FROM produit WHERE id = :id");
    $query->execute(["id" => $id]);
    header("Location: index.php");
}

function validateProductForm()
{
    $errors = [];
    if (empty($_POST["nom"])) {
        $errors[] = "Ajoutez un nom please !";
    }


    $priceFloat = floatval($_POST["price"]);

    if ($priceFloat == 0) {
        $errors[] = "Le prix n'est pas valide !";
    }

    if ($_POST["vente_flash"] != 'on' && $_POST["vente_flash"] != 'off') {
        $errors[] = "Arrete de modifier le HTML stp !!";
    }

    return $errors;
}

?>