<?php
require 'function/bdd-function.php';
require 'function/produit-function.php';

require "function/utilisateur-function.php";
checkAuthentification();

$bdd = bddConnect();




$idAsupprimer = $_GET["id"];

$produit = getProduitById($bdd, $idAsupprimer);

if(is_null($produit)){
    // renvoyer une page 404 le produit n'existe pas
} else {
    removeProductById($bdd, $idAsupprimer);

}

