<?php
require "function/utilisateur-function.php";
checkAuthentification();

    require "function/bdd-function.php";

    $bdd = bddConnect();

    $errors = [];
    if($_SERVER["REQUEST_METHOD"] == 'POST'){

        $errors = validateProductForm();
        $boolVenteFlash = ($_POST["vente_flash"] == 'on')? 1 : 0;

        if(count($errors) == 0){
            try {



                $query = $bdd->prepare("INSERT INTO produit (nom, price, vente_flash)
            VALUES (:nom, :price, :vente_flash)");

                $query->execute([
                   'nom'=>   $_POST["nom"],
                    'price'=> $_POST["price"],
                    "vente_flash" => $boolVenteFlash
                ]);

                header("Location: index.php");
            } catch (\PDOException $e){
                throw $e;
                die();
            }

        }

    }
?>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <?php
    include 'parts/global-stylesheets.php'
    ?>
</head>

<body>
<div class="container">
    <?php
    include 'parts/menu.php'
    ?>

    <h1>Ajouter un nouveau produit !</h1>

    <form action="add-produit.php" method="post">
        <form>
            <div class="form-group row">
                <label for="nom" class="col-sm-2 col-form-label">Nom</label>
                <div class="col-sm-10">
                    <input id="nom" type="text" name="nom" placeholder="Saisissez votre nom" class="form-control-plaintext">
                </div>
            </div>
            <div class="form-group row">
                <label for="price" class="col-sm-2 col-form-label">Prix</label>
                <div class="col-sm-10">
                    <input type="number"  step="0.01" class="form-control"
                           name="price"
                           id="price" placeholder="Prix du produit">
                </div>
            </div>

            <div class="form-group row">
                Produit flash :
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="vente_flash" value="on" id="isActifOui">
                    <label class="form-check-label" for="isActifOui">
                        Oui
                    </label>
                </div>

                <div class="form-check">

                    <input class="form-check-input" type="radio" name="vente_flash" value="off" id="actifNon" checked>
                    <label class="form-check-label" for="actifNon">
                        Non
                    </label>
                </div>



            </div>


            <input type="submit" class="btn btn-success">
        </form>
    </form>

    <?php
        foreach ($errors as $error){
            echo('<div class="alert alert-danger" role="alert">
  '.$error.'
</div>');
        }
    ?>
</div>
<?php
include 'parts/global-scripts.php';
?>
</body>

</html>
