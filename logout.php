<?php
require "function/utilisateur-function.php";
checkAuthentification();

session_destroy();
header("Location: login.php");