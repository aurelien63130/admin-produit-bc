<?php

    require 'function/bdd-function.php';
    require 'function/utilisateur-function.php';

    $bdd = bddConnect();


    $errors = [];

    if($_SERVER["REQUEST_METHOD"] == 'POST'){
        if(empty($_POST["email"])){
            $errors[] = "Veuillez saisir un email";
        }

        if(empty($_POST["password"])){
            $errors[] = "Veuillez saisir un mot de passe";
        }

        if(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
            $errors[] = "L'émail n'est pas valide";
        }

        if(count($errors) == 0){
            $user = getUtilisateurByMail($bdd, $_POST["email"]);

            if(count($user) == 0){
                $errors[]= "On te connais pas ici inscris toi STP";
            } else {
                if(password_verify($_POST["password"], $user[0]['mot_de_passe'])){
                    // On met utilisateur dans la session
                    $_SESSION["user"] = $user[0];
                    // On redirige notre utilisateur
                    header("Location: index.php");
                } else {
                    $errors[] = "Le mot de passe n'est pas bon !";
                }
            }

        }


    }
?>
<html>
<head>
    <?php
    include 'parts/global-stylesheets.php'
    ?>
</head>

<body>
<div class="container">

    <h1>Login</h1>


    <form method="post" action="login.php">
        <label for="email">Indiquez votre email</label>
        <input id="email" type="text" name="email" class="form-control">

        <label for="password">Mot de passe</label>
        <input id="password" type="password" name="password" class="form-control">

        <input type="submit" class="btn-success"><br>
    <a href="register.php">Créer un compte</a>

        <?php
        foreach ($errors as $error){
            echo('<div class="alert alert-danger" role="alert">
  '.$error.'
</div>');
        }
        ?>

</div>
<?php
include 'parts/global-scripts.php';
?>
</body>

</html>
