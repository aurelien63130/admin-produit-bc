<?php
require 'function/bdd-function.php';
require 'function/produit-function.php';

require "function/utilisateur-function.php";
checkAuthentification();

$bdd = bddConnect();
$idProduit = $_GET["id"];
$produit = getProduitById($bdd, $idProduit);

if(is_null($produit)){
    // Je renverrais une page 404 !
}

?>
<html>
<head>
    <?php
    include 'parts/global-stylesheets.php'
    ?>
</head>

<body>
<div class="container">
    <?php
    include 'parts/menu.php'
    ?>
    <h1>La page de : <?php echo($produit["nom"])?>! </h1>
    <span>Ce produit coute <?php echo($produit["price"]); ?> euros</span>
</div>
<?php
include 'parts/global-scripts.php';
?>
</body>

</html>
