<?php


require 'function/bdd-function.php';
require 'function/utilisateur-function.php';

$errors = [];

if($_SERVER["REQUEST_METHOD"] == 'POST'){
    if (empty($_POST["civilite"]) || $_POST["civilite"] != 'on' || $_POST["civilite"] != 'on') {
        $errors[] = 'La civilité n\'est pas valide';
    }

    if (empty($_POST["nom"])) {
        $errors[] = 'Veuillez saisir un nom !';
    }

    if (empty($_POST["prenom"])) {
        $errors[] = 'Veuillez saisir un prénom !';
    }

    if (empty($_POST["email"])) {
        $errors[] = 'Veuillez saisir un email !';
    }

    if (empty($_POST["email"])) {
        $errors[] = 'Veuillez saisir un email !';
    }

    if(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
        $errors[] = "L'émail n'est pas valide";
    }

    $bdd = bddConnect();

    if(count(getUtilisateurByMail($bdd, $_POST["email"])) != 0){
        $errors[] = "Ce mail est déjà lié à un compte ...";
    }

    if($_POST["mot_de_passe"] != $_POST["confirm_password"]){
        $errors[] = "Vos mots de passe ne sont pass cohérent";
    }

    try {
        $query = $bdd->prepare("INSERT INTO utilisateur (civilite, nom, prenom, email, mot_de_passe)
                    VALUES (:civilite, :nom, :prenom, :email, :mot_de_passe)");
        $query->execute([
            "nom"=> $_POST["nom"],
            "civilite"=> ($_POST["civilite"] == 'on')? 1 : 0,
            "prenom"=> $_POST["prenom"],
            "email"=> $_POST["email"],
            "mot_de_passe"=> password_hash($_POST["mot_de_passe"], PASSWORD_DEFAULT)
        ]);

        header("Location: login.php");
    } catch (\PDOException $e){
        $errors[] = 'Erreur inconnue ... ';
    }

}



?>

<html>
<head>
    <?php
    include 'parts/global-stylesheets.php'
    ?>
</head>

<body>
<div class="container">

    <h1>Créer mon compte</h1>

    <form method="post" action="register.php">
        <label>Civilité</label>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="civilite" id="monsieur">
            <label class="form-check-label" for="monsieur">
                Monsieur
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="civilite" id="madame" checked>
            <label class="form-check-label" for="madame">
                Madame
            </label>
        </div>


        <label for="nom">Nom</label>

        <input id="nom" type="text" name="nom" class="form-control">

        <label for="prenom">Prénom</label>

        <input id="prenom" type="text" name="prenom" class="form-control">


        <label for="email">Adresse Email</label>

        <input id="email" type="email" name="email" class="form-control">

        <label for="mot_de_passe">Mot de passe</label>

        <input id="mot_de_passe" type="password" name="mot_de_passe" class="form-control">

        <label for="confirm_password">Vérification du mot de passe</label>

        <input id="confirm_password" type="password" name="confirm_password" class="form-control">

        <input type="submit" class="btn-success">
    </form>

    <?php
    foreach ($errors as $error){
        echo('<div class="alert alert-danger" role="alert">
  '.$error.'
</div>');
    }
    ?>


    <a href="login.php">Finalement j'ai déjà un compte</a>
</div>
<?php
include 'parts/global-scripts.php';
?>
</body>

</html>
