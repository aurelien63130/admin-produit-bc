<?php
require "function/utilisateur-function.php";
checkAuthentification();

require 'function/bdd-function.php';
$bdd = bddConnect();

try {
    $bdd = new PDO("mysql:dbname=animalerie;host=database", "root", "tiger");
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (\Exception $e){
    echo('Impossible de se connecter ! ');
    throw $e;
}


$reponse = $bdd->query('SELECT * FROM produit');
$resultats = $reponse->fetchAll();

?>

<html>
<head>
    <?php
     include 'parts/global-stylesheets.php'
    ?>
</head>

<body>
<div class="container">
<?php
    include 'parts/menu.php'
?>
<h1>Les produits en BDD ! </h1>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nom</th>
            <th scope="col">Prix</th>
            <th scope="col">Est une vente flash ? </th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
            foreach ($resultats as $result){
                $venteFlash = 'Non';

                if($result["vente_flash"] == '1'){
                    $venteFlash = 'Oui';
                }

                echo ' <tr>
            <th scope="row">'.$result["id"].'</th>
            <td>'.$result["nom"].'</td>
            <td>'.$result["price"].' euros</td>
            <td>'.$venteFlash.'</td>
             <td>
       
             <a href="produit.php?id='.$result["id"].'"   data-bs-toggle="tooltip" title="Voir"><i class="fas fa-eye"></i> </a>
             <a href="delete.php?id='.$result["id"].'" data-bs-toggle="tooltip"  title="Supprimer">  <i class="fas fa-trash"></i> </a>
             <a href="update.php?id='.$result["id"].'" ><i class="fas fa-edit" data-container="body" data-bs-toggle="tooltip" title="Editer"></i> </a>
             </td>
        </tr>';
            }
        ?>
        </tbody>
    </table>
</div>
<?php
    include 'parts/global-scripts.php';
    ?>



<script rel="script" src="js/init_table.js"></script>

<script>
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
    console.log(tooltipTriggerList);

    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    })
</script>
</body>
</html>
